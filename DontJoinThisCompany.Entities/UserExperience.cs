//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DontJoinThisCompany.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserExperience
    {
        public int UserExperienceId { get; set; }
        public string UserExperienceText { get; set; }
        public int UserId { get; set; }
        public int CompanyId { get; set; }
        public System.DateTime DateCcreated { get; set; }
    
        public virtual UserProfile UserProfile { get; set; }
        public virtual CompanyDetail CompanyDetail { get; set; }
    }
}
