﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DontJoinThisCompany.Core.StringExtension
{
    public static class StringExtension
    {
        public static string ExtensionForString(this string inputString, int length)
        {
            if (inputString.Length <= length)
            {
                return inputString;
            }
            else
            {
                return string.Concat(inputString.Substring(0, length),"...");
            }
        }
    }
}
