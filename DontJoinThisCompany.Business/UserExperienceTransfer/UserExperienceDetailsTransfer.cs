﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Entities;
using DontJoinThisCompany.Models.CompanyAndUserExperience;
using DontJoinThisCompany.Business.UserExperienceQueries;
namespace DontJoinThisCompany.Business.UserExperienceTransfer
{
    public class UserExperienceDetailsTransfer
    {
        public void AddUserExperienceToDB(ShareUserExperienceModel model)
        {
            UserExperience user = new UserExperience();
            UserExperienceDetailsQueries addToDB = new UserExperienceDetailsQueries();
            user.UserExperienceText = model.UserExperience;
            user.UserId = model.UserId;
            user.CompanyId = model.CompanyId;
            user.DateCcreated = DateTime.Now;
            addToDB.InsertUserExperience(user);
        }
    }
}
