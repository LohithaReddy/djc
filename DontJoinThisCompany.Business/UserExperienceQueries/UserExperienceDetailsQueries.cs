﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.UserExperienceRepository;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
using DontJoinThisCompany.Models.CompanyAndUserExperience;
using DontJoinThisCompany.Business.UserRepository;
namespace DontJoinThisCompany.Business.UserExperienceQueries
{
    public class UserExperienceDetailsQueries
    {
        private IGenericRepository<Entities.UserExperience> genericRepository;
        private IGenericRepository<Entities.UserProfile> userRepository;
        public UserExperienceDetailsQueries()
        {
            this.genericRepository = new UserExperienceRepositoryImplement(new DontJoinThisCompanyEntities());
            this.userRepository = new UserRepositoryImplement(new DontJoinThisCompanyEntities());
        }
        public List<UserExperience> GetAllTheUserExperiences()
        {
            return genericRepository.GetAllData().ToList();
        }
        public UserExperience GetUserExperienceByUserID(int id)
        {
            return genericRepository.GetById(id);
        }
        public void InsertUserExperience(UserExperience insertData)
        {
            genericRepository.InsertData(insertData);
            genericRepository.Save();
        }
        public void UpdateUserExperience(UserExperience updateData)
        {
            genericRepository.UpdateData(updateData);
            genericRepository.Save();
        }
        public void DeletetUserExperience(UserExperience deleteData)
        {
            genericRepository.DeleteData(deleteData);
            genericRepository.Save();
        }
        public CompanyAndUserExperienceModel GetUserExperienceByCompanyId(int id)
        {
            var listOfUserexperiences = new CompanyAndUserExperienceModel();
            listOfUserexperiences.UserExperiences = new List<UserNameAndUserExperiences>();
            var UserExperiecesOfCompany = genericRepository.GetAllData().Where(x => x.CompanyId == id).OrderByDescending(x=>x.DateCcreated).ToArray();
            for (int i = 0; i < UserExperiecesOfCompany.Length; i++)
            {
                var userExperienceAndName = new UserNameAndUserExperiences();
                userExperienceAndName.UserFirstName = userRepository.GetById(UserExperiecesOfCompany[i].UserId).UserFirstName;
                userExperienceAndName.UserExperience = UserExperiecesOfCompany[i].UserExperienceText;
                listOfUserexperiences.UserExperiences.Add(userExperienceAndName);
            }
            return listOfUserexperiences;
        }
    }
}
