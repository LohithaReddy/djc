﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Business.UserRepository
{
    public class UserRepositoryImplement : IGenericRepository<Entities.UserProfile>, IDisposable
    {
        private DontJoinThisCompanyEntities Context;
        public UserRepositoryImplement(DontJoinThisCompanyEntities context)
        {
            this.Context = context;
        }

        public IQueryable<UserProfile> GetAllData()
        {
            return Context.UserProfiles;
        }

        public void InsertData(UserProfile insertData)
        {
            Context.UserProfiles.Add(insertData);
        }

        public void UpdateData(UserProfile updateData)
        {
            Context.Entry(updateData).State = System.Data.Entity.EntityState.Modified;
        }

        public void DeleteData(UserProfile deleteData)
        {
            Context.UserProfiles.Remove(deleteData);
        }
        public UserProfile GetById(int id)
        {
            return Context.UserProfiles.Find(id);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
