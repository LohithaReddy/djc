﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Business.UserExperienceRepository
{
    public class UserExperienceRepositoryImplement : IGenericRepository<UserExperience>, IDisposable
    {
        private DontJoinThisCompanyEntities Context;
        public UserExperienceRepositoryImplement(DontJoinThisCompanyEntities context)
        {
            this.Context = context;
        }
        public IQueryable<UserExperience> GetAllData()
        {
            return Context.UserExperiences;
        }

        public void InsertData(UserExperience insertData)
        {
            Context.UserExperiences.Add(insertData);
        }

        public void UpdateData(UserExperience updateData)
        {
            Context.Entry(updateData).State = System.Data.Entity.EntityState.Modified;
        }

        public void DeleteData(UserExperience deleteData)
        {
            Context.UserExperiences.Remove(deleteData);
        }

        public UserExperience GetById(int id)
        {
            return Context.UserExperiences.Find(id);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
