﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
using DontJoinThisCompany.Business.CompanyRepository;
using DontJoinThisCompany.Models.CopmanyViewModel;
namespace DontJoinThisCompany.Business.CompanyQueries
{
    public class CompanyDetailsQueries
    {
        private IGenericRepository<CompanyDetail> genericRepository;
        ComapanyDetailsViewModel homePageCompanies = new ComapanyDetailsViewModel();
        public CompanyDetailsQueries()
        {
            this.genericRepository = new CompanyRepositoryImplement(new DontJoinThisCompanyEntities());
        }
        public IQueryable<CompanyDetail> GetAllComapnies()
        {
            return genericRepository.GetAllData();
        }
        public List<CompanyDetail> GetRecentlyAddedcompanies()
        {
            return GetAllComapnies().OrderByDescending(x => x.DateCreated).Take(3).ToList();
        }
        public CompanyDetail GetCompanyById(int id)
        {
            return genericRepository.GetById(id);
        }
        public List<CompanyDetail> GetTopRatedCompanies()
        {
            return GetAllComapnies().OrderByDescending(x => x.CompanyRating).Take(6).ToList();
        }
        public List<CompanyDetail> GetAllCompanies()
        {
            return GetAllComapnies().OrderByDescending(x=>x.DateCreated).ToList();
        }
        public ComapanyDetailsViewModel GetComapiesForHomePage()
        {
            homePageCompanies.RecentlyAdded = GetRecentlyAddedcompanies();
            homePageCompanies.TopRated = GetTopRatedCompanies();
            return homePageCompanies;
        }
        public void UpdateCompanyRating(CompanyDetail company)
        {
            genericRepository.UpdateData(company);
            genericRepository.Save();
        }
        public List<CompanyDetail> GetCompanyDetailsByUSerId(int userId)
        {
            return GetAllComapnies().Where(x => x.UserId == userId).OrderByDescending(x=>x.DateCreated).ToList();
        }
        public void AddCompanyToDB(CompanyDetail company)
        {
            genericRepository.InsertData(company);
            genericRepository.Save();
        }
        public List<string> AutoCompleteforCompanies(string term)
        {
            return GetAllComapnies().Where(x => x.CompanyName.Contains(term)).Select(x=>x.CompanyName).ToList();
        }
        public List<CompanyDetail> CompaniesOnSearch(string serachKeyword)
        {
            return GetAllComapnies().Where(x => x.CompanyName.Contains(serachKeyword)).ToList();
        }
        public void UpdateCompanyDetails(CompanyDetail company)
        {
            genericRepository.UpdateData(company);
            genericRepository.Save();
        }
        public void DeleteCompany(int id)
        {
            genericRepository.DeleteData(GetCompanyById(id));
            genericRepository.Save();
        }
    }
}
