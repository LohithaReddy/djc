﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Models.CompanyForm;
using DontJoinThisCompany.Business.CompanyQueries;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Business.CompanyDetailsTransfer
{
   public class TransferCommpanyDetails
    {
       CompanyDetailsQueries companyAddtoDB = new CompanyDetailsQueries();
       public void AddCompanytoDB(AddNewCompanyToDB model)
       {
           CompanyDetail company = new CompanyDetail();
           company.CompanyName = model.CompanyName;
           company.CompanyDescription = model.CompanyDescription;
           company.CompanyAddress = model.CompanyAddress;
           company.CompanyEmailID = model.CompanyEmailID;
           company.CompanyTelephone = model.CompanyTelephone;
           company.CompanyRating = model.CompanyRating;
           company.UserId = model.UserId;
           company.DateCreated = DateTime.Now; ;
           companyAddtoDB.AddCompanyToDB(company);
       }
    }
}
