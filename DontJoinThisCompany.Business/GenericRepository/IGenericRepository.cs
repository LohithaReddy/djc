﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DontJoinThisCompany.Business.GenericRepository
{
    public interface IGenericRepository<T>
    {
        IQueryable<T> GetAllData();
        void InsertData(T insertData);
        void UpdateData(T updateData);
        void DeleteData(T deleteData);
        T GetById(int id);
        void Save();
    }
}
