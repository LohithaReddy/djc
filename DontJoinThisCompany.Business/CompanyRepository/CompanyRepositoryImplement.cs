﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Business.CompanyRepository
{
    public class CompanyRepositoryImplement : IGenericRepository<Entities.CompanyDetail>, IDisposable
    {
        private DontJoinThisCompanyEntities Context;
        public CompanyRepositoryImplement(DontJoinThisCompanyEntities context)
        {
            this.Context = context;
        }
        public IQueryable<CompanyDetail> GetAllData()
        {
            return Context.CompanyDetails;
        }

        public void InsertData(CompanyDetail insertData)
        {
            Context.CompanyDetails.Add(insertData);
        }

        public void UpdateData(CompanyDetail updateData)
        {
            Context.Entry(updateData).State = System.Data.Entity.EntityState.Modified;
        }

        public void DeleteData(CompanyDetail deleteData)
        {
            Context.CompanyDetails.Remove(deleteData);
        }
        public CompanyDetail GetById(int id)
        {
            return Context.CompanyDetails.Find(id);
        }
        public void Save()
        {
            Context.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
