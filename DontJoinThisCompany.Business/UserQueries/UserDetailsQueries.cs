﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Business.UserRepository;
using DontJoinThisCompany.Business.GenericRepository;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Business.UserQueries
{
  public  class UserDetailsQueries
    {
      private IGenericRepository<UserProfile> genericRepository;
      public UserDetailsQueries()
      {
          this.genericRepository = new UserRepositoryImplement(new DontJoinThisCompanyEntities());
      }
      public UserProfile GetUserByID(int id)
      {
          return genericRepository.GetById(id);
      }
      public void UpdateUserDetails(UserProfile user)
      {
          genericRepository.UpdateData(user);
          genericRepository.Save();
      }
      public void AddUser(UserProfile user)
      {
          genericRepository.InsertData(user);
          genericRepository.Save();
      }
     
    }
}
