﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Business.UserQueries;
using WebMatrix.WebData;
using DontJoinThisCompany.Models.UserLogin;
using DontJoinThisCompany.Web.Filters;
namespace DontJoinThisCompany.Web.Controllers.UserLoginForm
{
    [InitializeSimpleMembership]
    public class UserLoginDetailsController : Controller
    {
        // GET: /UserLoginDetails/
        UserDetailsQueries userName = new UserDetailsQueries();
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(UserLoginModel model)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.EmailAddress, model.Password))
            {
                try
                {
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "The Email Address or Password provided is incorrect");
                }
            }
            ModelState.AddModelError("", "The Email Address or password provided is incorrect.");
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}
