﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Business.CompanyRepository;
using DontJoinThisCompany.Business.CompanyQueries;
using WebMatrix.WebData;
using DontJoinThisCompany.Business.UserQueries;
using DontJoinThisCompany.Web.Filters;
namespace DontJoinThisCompany.Web.Controllers
{
     [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        UserDetailsQueries userName = new UserDetailsQueries();
        CompanyDetailsQueries companies = new CompanyDetailsQueries();
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    Session["username"] = userName.GetUserByID(WebSecurity.GetUserId(User.Identity.Name)).UserFirstName;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return View(companies.GetComapiesForHomePage());
        }

    }
}
