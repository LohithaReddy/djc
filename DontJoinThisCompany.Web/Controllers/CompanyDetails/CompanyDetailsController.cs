﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using DontJoinThisCompany.Business.CompanyQueries;
using DontJoinThisCompany.Models.CompanyAndUserExperience;
using DontJoinThisCompany.Business.UserExperienceQueries;
namespace DontJoinThisCompany.Web.Controllers.CompanyDetails
{
    public class CompanyDetailsController : Controller
    {
        
        CompanyDetailsQueries companyDetails = new CompanyDetailsQueries();
        public ActionResult AllCompanies(int? page)
        {
            return View(companyDetails.GetAllCompanies().ToPagedList(page ??1,15));
        }
        public ActionResult GetcompanyById(int id)
        {
            CompanyAndUserExperienceModel model = new CompanyAndUserExperienceModel();
            UserExperienceDetailsQueries userExperiences = new UserExperienceDetailsQueries();
            model = userExperiences.GetUserExperienceByCompanyId(id);
            model.CompanyDetails = companyDetails.GetCompanyById(id); 
            return View(model);
        }
        [HttpPost]
        public JsonResult UpdateRating(double RatedValue, int CompanyId)
        {
             if(Request.IsAuthenticated)
             {
                var company= companyDetails.GetCompanyById(CompanyId);
                company.CompanyRating = RatedValue;
                companyDetails.UpdateCompanyRating(company);
                return new JsonResult { Data = new { sucess = true, msg = "" } };
             }
             return new JsonResult { Data = new { sucess = false, msg = "Not updated" } };
        }

        public JsonResult AutoCompleteForCompanies(string term)
        {
            return  Json(companyDetails.AutoCompleteforCompanies(term), JsonRequestBehavior.AllowGet);
        }
    }
}
