﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Models.ManageUserSettings;
using DontJoinThisCompany.Business.UserQueries;
using WebMatrix.WebData;
using DontJoinThisCompany.Web.Filters;
using Postal;
namespace DontJoinThisCompany.Web.Controllers.ManageSettings
{
    [InitializeSimpleMembership]
    public class ManageUserSettingsController : Controller
    {
        //
        // GET: /ManageUserSettings/
        UserDetailsQueries userDetails = new UserDetailsQueries();
        public ActionResult ManageSettings()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UpdateUserName(ChangeUserName user)
        {
            if (Request.IsAuthenticated && ModelState.IsValid)
            {
                try
                {
                    var update = userDetails.GetUserByID(WebSecurity.GetUserId(User.Identity.Name));
                    update.UserFirstName = user.FirstName;
                    update.UserLastName = user.LastName;
                    userDetails.UpdateUserDetails(update);
                    return new JsonResult { Data = new { sucess = true, msg = "" } };
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "First name and Last name should contain atleast 2 characters");
                }
            }
            return new JsonResult { Data = new { sucess = false, msg = "First name and Last name should contain atleast 2 characters" } };
        }
        [HttpPost]
        public JsonResult UpdatePassword(ChangePassword user)
        {
            if (Request.IsAuthenticated && ModelState.IsValid)
            {
                try
                {
                    WebSecurity.ChangePassword(User.Identity.Name, user.CurrentPassword, user.NewPassword);
                    return new JsonResult { Data = new { sucess = true, msg = "" } };
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }
            return new JsonResult { Data = new { sucess = false, msg = "The current password is incorrect or the new password is invalid." } };
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgetPassword(string EmailAddress)
        {
            if (EmailAddress != null)
            {
                var validateEmailAddress = new System.Net.Mail.MailAddress(EmailAddress);
                if (validateEmailAddress.Address == EmailAddress)
                {
                    try
                    {
                        string passwordResetToken = WebSecurity.GeneratePasswordResetToken(EmailAddress,600);
                        dynamic email = new Email("EmailForResetPassword");
                        email.To = EmailAddress;
                        email.UserName = userDetails.GetUserByID(WebSecurity.GetUserId(EmailAddress)).UserFirstName;
                        email.PasswordResetToken = passwordResetToken;
                        email.Send();
                        return View("ResetPassword");
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "Oops! Something went wrong,Please try again");
                    }
                }
            }
            ModelState.AddModelError("", "Please enter the Email Address");
            return View();
        }
        public ActionResult MatchTokenForResetPassowrd(string passwordResetToken)
        {
            ViewBag.PasswordResetToken = passwordResetToken;
            return View();
        }
        [HttpPost]
        public ActionResult MatchTokenForResetPassowrd(ResetPasswordUsingToken resetPassword)
        {
            if (ModelState.IsValid && (resetPassword.NewPassword == resetPassword.ConfirmPassword))
            {
                try
                {
                    if (WebSecurity.ResetPassword(resetPassword.ResetToken, resetPassword.NewPassword) && WebSecurity.Login(resetPassword.EmailAddress, resetPassword.NewPassword))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "The new password and confirmation password do not match,Please try again!");
                }
            }
            else
            {
                ModelState.AddModelError("", "The new password and confirmation password do not match,Please try again!");
            }
            return View();

        }
    }
}
