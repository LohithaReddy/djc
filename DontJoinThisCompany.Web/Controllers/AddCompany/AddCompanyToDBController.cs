﻿using DontJoinThisCompany.Business.CompanyDetailsTransfer;
using DontJoinThisCompany.Models.CompanyForm;
using DontJoinThisCompany.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace DontJoinThisCompany.Web.Controllers.AddCompany
{
    [InitializeSimpleMembership]
    public class AddCompanyToDBController : Controller
    {
        TransferCommpanyDetails transfer = new TransferCommpanyDetails();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ActionName("Index")]
        public ActionResult AddCompany(AddNewCompanyToDB company)
        {
            if(Request.IsAuthenticated && ModelState.IsValid)
            {
                try
                {
                    company.UserId = WebSecurity.GetUserId(User.Identity.Name);
                    transfer.AddCompanytoDB(company);
                    return RedirectToAction("Index", "AddCompanyToDB");
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Invalid data,Please enter the correct data");
                }
            }
            return View(company);
        }

    }
}
