﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using DontJoinThisCompany.Business.CompanyQueries;
using DontJoinThisCompany.Business.UserQueries;
using WebMatrix.WebData;
using DontJoinThisCompany.Web.Filters;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Web.Controllers.UserActivityLog
{
    [InitializeSimpleMembership]
    public class ActivityLogController : Controller
    {
        //
        // GET: /ActivityLog/
        CompanyDetailsQueries details = new CompanyDetailsQueries();
        public ActionResult ActivityLog(int? page)
        {
            return View(details.GetCompanyDetailsByUSerId(WebSecurity.GetUserId(User.Identity.Name)).ToPagedList(page ?? 1, 10));
        }
        public ActionResult EditComapnyDetails(int id)
        {
            return View(details.GetCompanyById(id));
        }
        [HttpPost]
        public ActionResult EditComapnyDetails(CompanyDetail company)
        {
            if (Request.IsAuthenticated && ModelState.IsValid)
            {
                try
                {
                    details.UpdateCompanyDetails(company);
                    return RedirectToAction("ActivityLog");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Invalid data");
                }
            }
            return View(company);
        }
        public ActionResult PreviewCompanyDetails(int id)
        {
            return View(details.GetCompanyById(id));
        }
        [HttpPost]
        public void Deletecompany(int companyId)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    details.DeleteCompany(companyId);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "The details are not deleted,try again");
                }
            }

        }
    }
}
