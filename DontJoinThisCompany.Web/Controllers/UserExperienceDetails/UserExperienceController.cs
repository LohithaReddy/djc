﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Business.UserExperienceQueries;
using DontJoinThisCompany.Models.CompanyAndUserExperience;
using DontJoinThisCompany.Business.UserExperienceTransfer;
using WebMatrix.WebData;
using DontJoinThisCompany.Web.Filters;
namespace DontJoinThisCompany.Web.Controllers.UserExperienceDetails
{
    [InitializeSimpleMembership]
    public class UserExperienceController : Controller
    {
        //
        // GET: /UserExperience/

        [HttpPost]
        public JsonResult AddUserExperience(ShareUserExperienceModel userExperienceData)
        {
            if (Request.IsAuthenticated && ModelState.IsValid)
            {
                try
                {
                    UserExperienceDetailsTransfer userExperience = new UserExperienceDetailsTransfer();
                    userExperienceData.UserId = WebSecurity.GetUserId(User.Identity.Name);
                    userExperience.AddUserExperienceToDB(userExperienceData);

                    return new JsonResult { Data = new { sucess = true, msg = "" } };
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Invalid data");
                }
            }
            return new JsonResult { Data = new { sucess = false, msg = "Invalid data" } };
        }

    }
}
