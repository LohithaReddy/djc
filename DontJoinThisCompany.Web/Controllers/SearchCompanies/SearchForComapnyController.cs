﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Business.CompanyQueries;
namespace DontJoinThisCompany.Web.Controllers.SearchCompanies
{
    public class SearchForComapnyController : Controller
    {
        //
        // GET: /SearchForComapny/
        CompanyDetailsQueries company = new CompanyDetailsQueries();
        [HttpPost]
        public ActionResult SearchTheCompanies(string searchKeyword)
        {
            return View( company.CompaniesOnSearch(searchKeyword));
        }

    }
}
