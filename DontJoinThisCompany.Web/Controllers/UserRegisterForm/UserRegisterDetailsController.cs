﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DontJoinThisCompany.Models.UserLogin;
using DontJoinThisCompany.Models.UserRegister;

using DontJoinThisCompany.Business.UserRegisterDetailsTransfer;
using WebMatrix.WebData;
using System.Web.Security;
using DontJoinThisCompany.Web.Filters;
using Postal;

namespace DontJoinThisCompany.Web.Controllers.UserRegisterForm
{
    [InitializeSimpleMembership]
    public class UserRegisterDetailsController : Controller
    {
        //
        // GET: /UserRegisterDetails/
        UserDetailsTransfer user = new UserDetailsTransfer();
        public ActionResult RegisterForm()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RegisterForm(UserRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //var confirmationToken = Guid.NewGuid();
                    string confirmationToken = WebSecurity.CreateUserAndAccount(model.UserEmailID, model.Password, new { UserFirstName = model.UserFirstName, UserLastName = model.UserLastName }, true);
                    dynamic email = new Email("EmailConfirmedMsg");
                    email.To = model.UserEmailID;
                    email.UserName = model.UserFirstName;
                    email.ConfirmationToken = confirmationToken;
                    email.Send();
                    return RedirectToAction("RegisterStepTwo", "UserRegisterDetails");
                }
                catch (MembershipCreateUserException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult RegisterStepTwo()
        {
            return View();
        }
        public ActionResult RegisterConfirmation(string ConfirmationToken)
        {
            if (WebSecurity.ConfirmAccount(ConfirmationToken) && ConfirmationToken != null)
            {
                try
                {
                    return RedirectToAction("Login", "UserLoginDetails");
                }

                catch (Exception)
                {
                    ModelState.AddModelError("", "The registration has failed.Please try again");
                }
            }
            return RedirectToAction("ConfiramtionFailedRegisterForm", "UserRegisterDetails", new { errorMessage = "The registration has failed.Please try again" });
        }
        public ActionResult ConfiramtionFailedRegisterForm(string errorMessage)
        {
            if (errorMessage != null)
            {
                try
                {
                    ViewBag.ErrorMessage = errorMessage;
                    return View();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "The registration has failed.Please try again");
                }
            }
            ViewBag.ErrorMessage = "";
            return View();
        }
    }
}
