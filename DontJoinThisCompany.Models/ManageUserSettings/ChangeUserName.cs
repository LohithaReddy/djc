﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DontJoinThisCompany.Models.ManageUserSettings
{
    public class ChangeUserName
    {
        [Required(ErrorMessage="First name is requird")]
        [StringLength(50,MinimumLength=2)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is requird")]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }
    }
}
