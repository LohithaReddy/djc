﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DontJoinThisCompany.Models.CompanyAndUserExperience
{
   public class ShareUserExperienceModel
    {
       [Required(ErrorMessage="This field is required")]
       [StringLength(int.MaxValue,MinimumLength=10)]
       public string UserExperience { get; set; }
       public int UserId { get; set; }
       public int CompanyId { get; set; }
    }
}
