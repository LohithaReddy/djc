﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DontJoinThisCompany.Models.CompanyAndUserExperience
{
    public class UserNameAndUserExperiences
    {
        public string UserFirstName { get; set; }
        public string UserExperience { get; set; }
    }
}
