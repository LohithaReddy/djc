﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Models.CompanyAndUserExperience
{
    public class CompanyAndUserExperienceModel
    {
        public CompanyDetail CompanyDetails { get; set; }
        public List<UserNameAndUserExperiences> UserExperiences { get; set; }
    }
}
