﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DontJoinThisCompany.Entities;
namespace DontJoinThisCompany.Models.CopmanyViewModel
{
   public class ComapanyDetailsViewModel
    {
       public List<CompanyDetail> RecentlyAdded { get; set; }
       public List<CompanyDetail> TopRated { get; set; }
    }
}
