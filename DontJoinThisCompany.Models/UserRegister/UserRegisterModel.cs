﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DontJoinThisCompany.Models.UserRegister
{
    public class UserRegisterModel
    {
        public int UserProfileID { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(30)]
        public string UserFirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(30)]
        public string UserLastName { get; set; }
        [Required(ErrorMessage = "Email Address is required")]
        [DataType(DataType.EmailAddress)]
        public string UserEmailID { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
