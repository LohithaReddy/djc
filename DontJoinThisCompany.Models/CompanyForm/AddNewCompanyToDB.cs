﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DontJoinThisCompany.Models.CompanyForm
{
    public class AddNewCompanyToDB
    {
        [Required(ErrorMessage = "Company Name is required")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "company Description is required")]
        public string CompanyDescription { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyEmailID { get; set; }
        public string CompanyTelephone { get; set; }
        [Required(ErrorMessage = "Email Address is required")]
        public double CompanyRating { get; set; }     
        public int UserId { get; set; }
    }
}
